#encoding: utf-8


from flask import Flask,render_template,url_for,g
import config
import requests
import sys
from datetime import datetime

reload(sys)

sys.setdefaultencoding('utf8')
app=Flask(__name__)
app.config.from_object(config)

@app.route('/')
def index():

	return render_template('index.html',r=g.r,now=g.now,day=g.day)

@app.route('/my_msg')
def my_msg():
	return render_template('my_msg.html')

@app.route('/hoppy')
def hoppy():
	return	render_template('hoppy.html')

@app.route('/evaluation')
def evaluation():
	return render_template('evaluation.html')

@app.route('/apply')
def apply():
	return render_template('apply.html')

@app.before_request
def my_before_request():
	
	headers={
		'Host':'d1.weather.com.cn',	
		'User-Agent':'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.79 Safari/537.36',
		'Referer':'http://www.weather.com.cn/weather/101280101.shtml',		
	}
	result=requests.get('http://d1.weather.com.cn/dingzhi/101280101.html?_=1509697675841',headers=headers)
	res=result.content.split("=")[1].split(";")[0]
	r=eval(res)
	g.r=r
#当前时间
	time=datetime.now()
	now=time.strftime('%Y-%m-%d %H:%M')
	g.now=now  
#今天星期几	
	weeks=time.weekday()
	week_dict={
		0:'星期一',1:'星期二',
		2:'星期三',3:'星期四',
		4:'星期五',5:'星期六',
		6:'星期日',
	}
	day=week_dict[weeks]
	g.day=day


if __name__=="__main__":
	app.run(host='0.0.0.0', port=5001)


